import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import Reducers from './reducers';

const store = createStore(Reducers, applyMiddleware( promise(), thunk));

export default store;
