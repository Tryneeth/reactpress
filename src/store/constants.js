const Constants = {
	actionTypes: {
		FETCH_ALL_POSTS: 'FETCH_ALL_POSTS',
		FETCH_ALL_PAGES: 'FETCH_ALL_PAGES',
		FETCH_POST: 'FETCH_POST',
		FETCH_PAGE: 'FETCH_PAGE',
	},
	config: {
		baseAPIUrl: 'http://localhost/wordpress/wp-json/wp/v2',
		errorMessage: 'Please make sure you Wordpress API is Accessible'
	}
};

export default Constants;
