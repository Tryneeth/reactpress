import axios from 'axios';
import Constants from './constants';

const instance = axios.create({
	baseURL: Constants.config.baseAPIUrl,
});

instance.interceptors.request.use((config) => {
	return config;
});

export default instance;