import Constants from '../constants';
import instance from '../axios.config';

const PostsAction = {
	getPost(id) {
		return {
			type: Constants.actionTypes.FETCH_POST,
			payload: instance.get(`/posts/${id}?_embed=true`)
		}
	},
	getPosts(numOfPosts = 0) {
		const perPage = numOfPosts > 0 ? `&per_page=${numOfPosts}` : ``;
		return {
			type: Constants.actionTypes.FETCH_ALL_POSTS,
			payload: instance.get(`/posts?_embed=true${perPage}`)
		}
	}
};

export default PostsAction;