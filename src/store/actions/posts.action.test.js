import configureStore from 'redux-mock-store';
import promiseMiddleware from 'redux-promise-middleware';
import moxios from 'moxios';
import postsAction  from './posts.action';
import instance from '../axios.config';
import Constants from '../constants';

describe('async actions', () => {
	const middlewares = [promiseMiddleware()];
	const mockStore = configureStore(middlewares);

	beforeEach(() => {
		moxios.install(instance);
	});
	afterEach(() => {
		moxios.uninstall(instance);
	});
	it(`it dispatches ${Constants.actionTypes.FETCH_ALL_POSTS}_FULFILLED and ${Constants.actionTypes.FETCH_ALL_POSTS}_PENDING on fetch posts`, () => {
		const payload = {
			posts: [] };
		moxios.wait(() => {
			const request = moxios.requests.mostRecent();
			request.respondWith({
				status: 200,
				response: payload,
			});
		});
		const expectedActions = [`${Constants.actionTypes.FETCH_ALL_POSTS}_PENDING`, `${Constants.actionTypes.FETCH_ALL_POSTS}_FULFILLED`];
		const store = mockStore({ posts: [] });

		return store.dispatch(postsAction.getPosts()).then(() => {

				const dispatchedActions = store.getActions();
				const actionTypes = dispatchedActions.map(action => action.type);

				expect(actionTypes).toEqual(expectedActions);
			},
		);
	});
});