import { combineReducers } from 'redux';
import postsReducer from './posts.reducer';
import postReducer from './post.reducer';

const Reducer = combineReducers({
	post: postReducer,
	posts: postsReducer,
});

export default Reducer;