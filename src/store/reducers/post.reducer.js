import Constants from '../constants';

export const POST_DEFAULT_STATE = {
	fetchPostLoading: true,
	fetchPostError: {},
	error: false,
	post: {},
};

export default function postReducer(state = POST_DEFAULT_STATE, action) {
	switch (action.type) {
		case `${Constants.actionTypes.FETCH_POST}_PENDING`:
			return {
				...state,
				fetchPostLoading: true,
			};
		case `${Constants.actionTypes.FETCH_POST}_FULFILLED`:
			return {
				...state,
				fetchPostLoading: false,
				post: action.payload.data,
			};
		case `${Constants.actionTypes.FETCH_POST}_REJECTED`:
			return {
				...state,
				fetchPostLoading: false,
				error: true,
				fetchPostError: action.payload,
			};
		default:
			return state;
	}
}