import Constants from '../constants';

export const POSTS_DEFAULT_STATE = {
	fetchPostsLoading: false,
	fetchPostsError: {},
	error: false,
	posts: {},
};

export default function postsReducer(state = POSTS_DEFAULT_STATE, action) {
	switch (action.type) {
		case `${Constants.actionTypes.FETCH_ALL_POSTS}_PENDING`:
			return {
				...state,
				fetchPostsLoading: true,
			};
		case `${Constants.actionTypes.FETCH_ALL_POSTS}_FULFILLED`:
			return {
				...state,
				fetchPostsLoading: false,
				posts: action.payload.data,
			};
		case `${Constants.actionTypes.FETCH_ALL_POSTS}_REJECTED`:
			return {
				...state,
				fetchPostsLoading: false,
				error: true,
				fetchPostsError: action.payload,
			};
		default:
			return state;
	}
}