import postReducer, { POST_DEFAULT_STATE } from './post.reducer';
import Constants from '../constants';

describe('Post Reducer Test Suite', () => {
	it('Should return the default state', () => {
		const state = postReducer(undefined, { type: 'TEST' });
		expect(state).toEqual(POST_DEFAULT_STATE);
	});

	test(`Should handle ${Constants.actionTypes.FETCH_POST}_PENDING`, () => {
		const state = {};
		const action = {
			type: `${Constants.actionTypes.FETCH_POST}_PENDING`,
			fetchPostLoading: true,
		};
		expect(postReducer(state, action)).toEqual({
			fetchPostLoading: true,
		});
	});

	test(`Should handle ${Constants.actionTypes.FETCH_POST}_FULFILLED`, () => {
		const state = {};
		const expectedState = {
			fetchPostLoading: false,
			post: {},
		};

		const action = {
			type: `${Constants.actionTypes.FETCH_POST}_FULFILLED`,
			payload: { data: {} },
		};
		expect(postReducer(state, action)).toEqual(expectedState);
	});

	test(`Should handle ${Constants.actionTypes.FETCH_POST}_REJECTED`, () => {
		const state = {};
		const expectedState = {
			fetchPostLoading: false,
			fetchPostError: {},
			error: true
		};

		const action = {
			type: `${Constants.actionTypes.FETCH_POST}_REJECTED`,
			payload: { },
		};
		expect(postReducer(state, action)).toEqual(expectedState);
	});

});
