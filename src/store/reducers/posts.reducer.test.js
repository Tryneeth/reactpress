import postsReducer, { POSTS_DEFAULT_STATE } from './posts.reducer';
import Constants from '../constants';

describe('Posts Reducer Test Suite', () => {
	it('Should return the default state', () => {
		const state = postsReducer(undefined, { type: 'TEST' });
		expect(state).toEqual(POSTS_DEFAULT_STATE);
	});

	test(`Should handle ${Constants.actionTypes.FETCH_ALL_POSTS}_PENDING`, () => {
		const state = {};
		const action = {
			type: `${Constants.actionTypes.FETCH_ALL_POSTS}_PENDING`,
			fetchPostsLoading: true,
		};
		expect(postsReducer(state, action)).toEqual({
			fetchPostsLoading: true,
		});
	});

	test(`Should handle ${Constants.actionTypes.FETCH_ALL_POSTS}_FULFILLED`, () => {
		const state = {};
		const expectedState = {
			fetchPostsLoading: false,
			posts: {},
		};

		const action = {
			type: `${Constants.actionTypes.FETCH_ALL_POSTS}_FULFILLED`,
			payload: { data: {} },
		};
		expect(postsReducer(state, action)).toEqual(expectedState);
	});

	test(`Should handle ${Constants.actionTypes.FETCH_ALL_POSTS}_REJECTED`, () => {
		const state = {};
		const expectedState = {
			fetchPostsLoading: false,
			fetchPostsError: {},
			error: true
		};

		const action = {
			type: `${Constants.actionTypes.FETCH_ALL_POSTS}_REJECTED`,
			payload: { },
		};
		expect(postsReducer(state, action)).toEqual(expectedState);
	});

});
