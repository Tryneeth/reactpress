import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import LoadingPage from './LoadingPage';

Enzyme.configure({adapter: new Adapter()});

function setup(){

	const enzymeWrapper = mount(<LoadingPage />);

	return {
		enzymeWrapper
	}
}

describe('Components', () => {
	describe("LoadingPage", () => {
		it('should render self and subcomponents', () => {
			const { enzymeWrapper } = setup();
			expect(enzymeWrapper.find('div.loading-overlay').hasClass('d-flex')).toBe(true);
		});
	});
});