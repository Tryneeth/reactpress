import React from 'react';
import './Error.css'

const Error = (props) => {
	return (
		<div className='error-overlay d-flex justify-content-center align-items-center'>
			<div className='error-message'>
				<h1 className='text-white'>{props.header}</h1>
				<h3 className='text-white'>{props.message}</h3>
			</div>
		</div>
	);
};

export default Error;