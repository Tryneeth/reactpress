import React from 'react';
import { Animation } from 'mdbreact';
import './LoadingPage.css';

const LoadingPage = () => {
	return (
		<div className='loading-overlay d-flex justify-content-center align-items-center'>
			<Animation type="zoomIn" infinite>
				<div className="loading-circle">
				</div>
			</Animation>
		</div>
	);
};

export default LoadingPage;