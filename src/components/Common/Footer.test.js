import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Footer from './Footer';

Enzyme.configure({adapter: new Adapter()});

function setup(){

	const enzymeWrapper = mount(<Footer />);

	return {
		enzymeWrapper
	}
}

describe('Components', () => {
	describe("Footer", () => {
		it('should render self and subcomponents', () => {
			const { enzymeWrapper } = setup();
			expect(enzymeWrapper.find('footer').hasClass('d-flex')).toBe(true);
		});
	});
});

