import React from 'react';
import { Container, Row } from 'mdbreact';

const PageContainer = (props) => {
	return (
		<Container className="page-container mb-5">
			<Row>
				{props.children}
			</Row>
		</Container>
	);
};

export default PageContainer;

