import React from 'react'
import './Footer.css';

const Footer = () => {
	return(
		<footer className="d-flex justify-content-center align-items-center">
			<h6 className="text-white">Copyright of Aleph Engineering. Made by Escobar</h6>
		</footer>
	)
};

export default Footer
