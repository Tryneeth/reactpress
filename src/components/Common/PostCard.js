import React, { Component } from 'react';
import { Card, CardBody, CardImage, CardText, CardTitle, CardFooter } from 'mdbreact';
import { Link } from 'react-router-dom';
import './PostCard.css';

export default class PostCard extends Component {

	getAttachment(post) {
		return post._embedded['wp:featuredmedia'][0].source_url;
	}

	render() {
		let post = this.props.post;
		let content = this.props.content ? post.content.rendered : post.excerpt.rendered;
		let singlePost = this.props.singlePost ? this.props.singlePost : false;
		let className = this.props.singlePost ? 'text-center' : '';

		let button = this.props.singlePost
			? <Link className="btn btn-elegant" to='/posts'>Watch All Posts</Link>
			: <Link className="btn btn-elegant" to={`/post/${post.id}`}>Read More</Link>;
		let footerClass = this.props.singlePost
			? 'text-center single-post-card-footer'
			: 'text-center';

		return (
			<Card reverse={singlePost}>
				<CardImage cascade={singlePost} className="img-fluid" src={this.getAttachment(post)}/>
				<CardBody cascade={singlePost} className={className}>
					<CardTitle dangerouslySetInnerHTML={{ __html: post.title.rendered }} />
					<CardText className="text-left" dangerouslySetInnerHTML={{ __html: content }}/>
				</CardBody>
				<CardFooter className={footerClass}>
					{button}
				</CardFooter>
			</Card>
		);
	}
}