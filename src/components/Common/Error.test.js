import React from 'react';
import Enzyme, {mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Error from './Error';

Enzyme.configure({adapter: new Adapter()});

function setup(){
	const props = {
		header: "Header text",
		message: "Message"
	};

	const enzymeWrapper = mount(<Error {...props}/>);

	return {
		props,
		enzymeWrapper
	}
}

describe('Components', () => {
	describe("Error", () => {
		it('should render self and subcomponents', () => {
			const { enzymeWrapper } = setup();
			expect(enzymeWrapper.find('h1').hasClass('text-white')).toBe(true);
			const errorProps = enzymeWrapper.find('Error').props();
			expect(errorProps.header).toEqual("Header text");
			expect(errorProps.message).toEqual("Message");
		});
	});
});

