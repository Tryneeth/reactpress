import React from 'react';
import './HeroSection.css';
import heroBG from '../../assets/hero-bg.jpg';
import { Fa, Animation } from 'mdbreact';

const HeroSection = () => {

	return (
		<section className='hero'>
			<header id="masthead" className="site-header" role="banner">

				<div className="custom-header">

					<div className="custom-header-media">
						<div id="custom-header" className="custom-header"><img
							src={heroBG} width="2000"
							height="1200" alt="ReactPress"/></div>
					</div>

					<div className="site-branding">
						<div className="wrap">

							<div className="site-branding-text">
								<h1 className="site-title"><a href="http://localhost/wordpress/" rel="home">ReactPress</a></h1>
								<p className="site-description">React consumer for Worpdress API</p>
							</div>

							<div className="float-right mt-4">
								<Animation type="slideInDown" infinite>
									<Animation type="fadeIn" infinite>
										<Fa className="text-white fa-3x" icon="chevron-down"/>
									</Animation>
								</Animation>
							</div>

						</div>
					</div>

				</div>

			</header>
		</section>
	);
};

export default HeroSection;