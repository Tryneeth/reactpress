import React from 'react';
import {
	Container,
	Row,
	Col,
	CardGroup,
} from 'mdbreact';
import PostCard from '../Common/PostCard';

const LastNewsSection = (props) => {

	const posts = props.posts;
	return (
		<Container className="my-5">
			<Row className="text-center">
				<Col sm="12" className="mb-5">
					<h1 className="text-uppercase font-weight-bold">Last News</h1>
					<hr className="title-border" />
				</Col>
			</Row>
			<Row>
				<CardGroup deck>
					{posts && Object.values(posts).map((post) => {
						return (
							<PostCard key={post.id} post={post}/>
						);
					})}
				</CardGroup>
			</Row>
		</Container>
	);
};

export default LastNewsSection;