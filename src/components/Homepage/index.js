import React, { Component } from 'react';
import connect from 'react-redux/es/connect/connect';
import HeroSection from './HeroSection';
import ServicesSection from './ServicesSection';
import PostsAction from '../../store/actions/posts.action';
import LastNewsSection from './LastNewsSection';
import LoadingPage from '../Common/LoadingPage';
import Error from '../Common/Error';
import Constants from '../../store/constants';

class Homepage extends Component {

	componentDidMount() {
		this.props.getPosts(3);
	}

	render() {
		if(this.props.fetchPostsLoading)
			return(
				<LoadingPage/>
			);
		else if (this.props.error) {
			return (
				<Error header={this.props.fetchPostsError.message} message={Constants.config.errorMessage}/>
			);
		}
		else
		return (
			<React.Fragment>
				<HeroSection/>
				<LastNewsSection posts={this.props.posts}/>
				<ServicesSection/>
			</React.Fragment>
		);
	}

}

export default connect(store => {
	return {
		fetchPostsLoading: store.posts.fetchPostsLoading,
		fetchPostsError: store.posts.fetchPostsError,
		posts: store.posts.posts,
		error: store.posts.error,
	};
}, dispatch => {
	return {
		getPosts: (per_page) => {
			dispatch(PostsAction.getPosts(per_page));
		},
	};
})(Homepage);