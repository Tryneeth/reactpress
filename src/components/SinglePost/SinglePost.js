import React, { Component } from 'react';
import PostCard from '../Common/PostCard';
import connect from 'react-redux/es/connect/connect';
import PostsAction from '../../store/actions/posts.action';
import PageContainer from '../Common/PageContainer';
import LoadingPage from '../Common/LoadingPage';
import Error from '../Common/Error';
import Constants from '../../store/constants';

class SinglePost extends Component {

	componentDidMount() {
		this.props.getPost(this.props.match.params.id);
	}

	render() {
		if (this.props.fetchPostLoading)
			return (
				<LoadingPage/>
			);
		else if (this.props.error) {
			return (
				<Error header={this.props.fetchPostError.message} message={Constants.config.errorMessage}/>
			);
		}
		else
			return (
				<PageContainer>
					<PostCard singlePost={true} fullWidth key={this.props.post.id} content={true} post={this.props.post}/>
				</PageContainer>
			);
	}
}

export default connect(store => {
	return {
		fetchPostLoading: store.post.fetchPostLoading,
		fetchPostError: store.post.fetchPostError,
		error: store.post.error,
		post: store.post.post,
	};
}, dispatch => {
	return {
		getPost: (id) => {
			dispatch(PostsAction.getPost(id));
		},
	};
})(SinglePost);