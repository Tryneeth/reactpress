import React, { Component } from 'react';
import {
	Navbar,
	NavbarBrand,
	NavbarNav,
	NavbarToggler,
	Collapse,
	NavItem,
	NavLink,
} from 'mdbreact';

export default class NavbarFeatures extends Component {
	constructor(props) {
		super(props);
		this.state = {
			collapse: false,
			isWideEnough: false,
		};
		this.onClick = this.onClick.bind(this);
	}

	onClick() {
		this.setState({
			collapse: !this.state.collapse,
		});
	}

	render() {
		return (
			<Navbar color="elegant-color" dark expand="md" fixed="top">
				<NavbarBrand to="/">
					<strong className="text-white">Navbar</strong>
				</NavbarBrand>
				{!this.state.isWideEnough && <NavbarToggler onClick={this.onClick}/>}
				<Collapse isOpen={this.state.collapse} navbar>
					<NavbarNav left>
						<NavItem>
							<NavLink to="/">Home</NavLink>
						</NavItem>
						<NavItem>
							<NavLink to="/posts">All News</NavLink>
						</NavItem>
						<NavItem>
							<NavLink to="/about">About</NavLink>
						</NavItem>
					</NavbarNav>
				</Collapse>
			</Navbar>
		);
	}
}