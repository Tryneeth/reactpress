import React from 'react';
import PostCard from '../Common/PostCard';

const Posts = (props) => {
	return (
		<PostCard post={props.post}/>
	);
};

export default Posts;