import React, { Component } from 'react';
import connect from 'react-redux/es/connect/connect';
import PostsAction from '../../store/actions/posts.action';
import {
	Col,
} from 'mdbreact';
import Posts from './Posts';
import PageContainer from '../Common/PageContainer';
import LoadingPage from '../Common/LoadingPage';
import Error from '../Common/Error';
import Constants from '../../store/constants';

class AllNews extends Component {

	componentDidMount() {
		this.props.getPosts();
	}

	render() {
		if (this.props.fetchPostsLoading)
			return (
				<LoadingPage/>
			);
		else if (this.props.error) {
			return (
				<Error header={this.props.fetchPostsError.message} message={Constants.config.errorMessage}/>
			);
		}
		else {
			const posts = this.props.posts;
			return (
				<PageContainer>
						<Col sm="12" className="mb-5 text-center">
							<h1 className="text-uppercase font-weight-bold">All News</h1>
							<hr className="title-border" />
						</Col>
					{
						posts && Object.values(posts).map(post => {
							return (
								<React.Fragment key={post.id}>
									<Col className="mb-2" sm="12" md="6" lg="4">
										<Posts post={post}/>
									</Col>
								</React.Fragment>
							);
						})
					}
				</PageContainer>
			);
		}
	}
}

export default connect(store => {
	return {
		fetchPostsLoading: store.posts.fetchPostsLoading,
		fetchPostsError: store.posts.fetchPostsError,
		error: store.posts.error,
		posts: store.posts.posts,
	};
}, dispatch => {
	return {
		getPosts: () => {
			dispatch(PostsAction.getPosts());
		},
	};
})(AllNews);