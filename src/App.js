import React from 'react';
import 'font-awesome/css/font-awesome.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
import './App.css';
import NavigationBar from './components/NavigationBar';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Homepage from './components/Homepage';
import AllNews from './components/AllNews';
import SinglePost from './components/SinglePost/SinglePost';
import Footer from './components/Common/Footer';

const App = () => {
	return (
		<BrowserRouter>
			<React.Fragment>
				<NavigationBar/>
				<Switch>
					<Route path='/' exact component={Homepage}/>
					<Route path='/posts' component={AllNews}/>
					<Route path='/post/:id' component={SinglePost}/>
				</Switch>
				<Footer/>
			</React.Fragment>
		</BrowserRouter>
	);
};

export default App;